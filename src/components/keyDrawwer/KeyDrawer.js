import React, {Component} from 'react';
import DrawPaper from "./DrawPaper";
import {
    deepCopy,
    pair,
    point,
    toDouble,
    toInt
} from "../../helpers/Helper";
import Cdiv from "../Cdiv";
import Button from "@material-ui/core/Button";
import SettingsIcon from "@material-ui/icons/Settings";
import IconButton from "@material-ui/core/IconButton";
import SettingsDialog from "./SettingsDialog";
import ToggleButton from "@material-ui/lab/ToggleButton";
import {emptyObjPicture, keyFromPoint, objPictureToArray} from "../../helpers/KeyDrawerHelper";
import {diag, dist, safeDiv} from "../../helpers/MathHelper";

const isLocalHost = ['127.0.0.1', 'localhost'].includes(location.hostname);
const socketHost = isLocalHost ? 'localhost:9000/' : location.href;

// const socket = new WebSocket('ws://' + socketHost + "api/socket");
// socket.onclose = () => console.log('closed!');
// socket.onerror = e => console.log('error', e);

const SETT_KEY = 'keyDrawerSettings';

const DEFAULT_SIZE = 600;

export default class KeyDrawer extends Component {

    state = {
        showSettDialog: false,
        eraser: false,
        settings: {
            W: '' + 28,
            H: '' + 28,
            brushRadiusCoef: '' + 1.5,
            withBorders: false,
            black: true
        },
        picture: emptyObjPicture(28, 28),
    };

    saveSett = () => {
        localStorage.setItem(SETT_KEY, JSON.stringify(this.state.settings));
    };

    restoreSett = () => {
        const settStringified = localStorage.getItem(SETT_KEY);
        const settings = settStringified ? JSON.parse(settStringified) : this.state.settings;
        console.log(settings);
        this.setState({settings});
        setTimeout(this.clear, 500);
    };

    componentDidMount() {
        this.restoreSett();
        // socket.onmessage = m => console.log('opened!', m);
        // socket.onopen = () => {
        //     socket.send('testtt');
        //     console.log('opened!');
        // };
    };

    onSettChange = (key, value) => {
        const settings = deepCopy(this.state.settings);
        settings[key] = value;
        this.setState({settings});
        setTimeout(() => {
            if (key === 'W' || key === 'H')
                this.clear();
            this.saveSett();
        }, 500);
    };

    pixCenter = (i, j, pixelSize) => [(i + 0.5) * pixelSize, (j + 0.5) * pixelSize];

    distWeight = (mouseX, mouseY, pixelSize) => (i, j) => {
        const [pixelCenterX, pixelCenterY] = this.pixCenter(i, j, pixelSize);
        const radius = this.brushRadCoef() * pixelSize;
        const dst = dist(point(mouseX, mouseY), point(pixelCenterX, pixelCenterY));
        const distWeight = Math.max(0, 1 - safeDiv(dst, radius));
        const powered = Math.min(1, Math.pow(distWeight, 1/4));
        return powered;
    };

    nearbyPixels = (pixX, pixY) => {
        const points = [];
        for (let i = Math.max(pixX - 2, 0); i <= Math.min(pixX + 2, this.H() - 1); ++i)
            for (let j = Math.max(pixY - 2, 0); j <= Math.min(pixY + 2, this.W() - 1); ++j)
                points.push(point(i, j));
        return points;
    };

    newPix = (distW, isEraser) => pixel => {
        const calcVal = distW(pixel.x, pixel.y);
        const key = keyFromPoint(pixel);
        const newV = (calcVal > 0 && isEraser) ? 0 : Math.max(calcVal, this.state.picture[key]);
        return pair(key, newV);
    };

    draw = (mouseX, mouseY, isEraser) => {
        isEraser = isEraser || this.state.eraser;
        const pixelSize = (this.props.size || DEFAULT_SIZE) / this.W();

        const distW = this.distWeight(mouseX, mouseY, pixelSize);
        const mousePixX = Math.floor(mouseX / pixelSize);
        const mousePixY = Math.floor(mouseY / pixelSize);

        const newImPart = this.nearbyPixels(mousePixX, mousePixY)
            .map(this.newPix(distW, isEraser))
            .reduceRight((p, c) => ({...p, ...c}), {});

        const picture = {...this.state.picture, ...newImPart};

        this.props.onDraw(objPictureToArray(deepCopy(picture)));
        this.setState({picture});
    };

    H = () => toInt(this.state.settings.H);
    W = () => toInt(this.state.settings.W);
    brushRadCoef = () => toDouble(this.state.settings.brushRadiusCoef);

    clear = () => this.setState({picture: emptyObjPicture(this.W(), this.H())});

    onSave = () => {
        this.props.onSave(objPictureToArray(this.state.picture));
        this.clear();
    };

    toggleEraser = () => {
        this.setState({eraser: !this.state.eraser});
    };

    render() {

        const {size} = this.props;

        const {showSettDialog, picture, settings, eraser} = this.state;


        return (
            <Cdiv style={{width: '100%', height: '100%', flexDirection: 'column'}} x y className="App">
                <Cdiv>
                    <Button onClick={this.onSave}>Save</Button>
                    <Button onClick={this.clear}>Clear</Button>
                    <ToggleButton selected={eraser}
                                  style={{fontWeight: eraser ? 'bold' : '100'}}
                                  onChange={this.toggleEraser}>Eraser</ToggleButton>
                    <IconButton onClick={() => this.setState({showSettDialog: true})}><SettingsIcon/></IconButton>
                </Cdiv>
                <SettingsDialog
                    settings={settings}
                    onChange={this.onSettChange}
                    show={showSettDialog}
                    onClose={() => this.setState({showSettDialog: false})}/>
                <DrawPaper onDraw={this.draw}
                           size={size || DEFAULT_SIZE}
                           settings={settings}
                           picture={picture}/>
            </Cdiv>
        );
    }
}

