import Dialog from "@material-ui/core/Dialog";
import React from "react";
import {toInt} from "../../helpers/Helper";
import Cdiv from "../Cdiv";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";
import DialogContent from "@material-ui/core/DialogContent";
import {emptyObjPicture} from "../../helpers/KeyDrawerHelper";

const LabeledCheckbox = ({checked, onChange, label, checkboxProps, ...other}) => {
    return (<FormControlLabel
        {...other}
        control={
            <Checkbox
                {...checkboxProps}
                checked={checked}
                onChange={e => onChange(e.target.checked)}
                color="primary"
            />
        }
        label={label}
    />);
};

const SettingsDialog = (props) => {
    const {settings, show, onClose, onChange} = props;

    const {W, H, brushRadiusCoef, withBorders, black} = settings;

    return (<Dialog open={show} onClose={onClose}>
        <DialogTitle>
            <Typography>Settings</Typography>
        </DialogTitle>
        <DialogContent>
            <div>
                <TextField label="Width"
                           fullWidth
                           type='number'
                           helperText="Pixels count by X axes"
                           value={W}
                           disabled
                           onChange={e => onChange('W', e.target.value)}/>
                <TextField label="Height"
                           fullWidth
                           type='number'
                           helperText="Pixels count by Y axes"
                           value={H}
                           disabled
                           onChange={e => onChange('H', e.target.value)}/>
                <TextField label="Brush bold"
                           fullWidth
                           type='number'
                           helperText="Brush boldness"
                           value={brushRadiusCoef}
                           onChange={e => onChange('brushRadiusCoef', e.target.value)}/>
                <LabeledCheckbox checked={withBorders}
                                 fullWidth
                                 onChange={v => onChange('withBorders', v)}
                                 label="With borders"
                />
                <LabeledCheckbox checked={black}
                                 fullWidth
                                 onChange={v => onChange('black', v)}
                                 label="Black"
                />
            </div>
        </DialogContent>
    </Dialog>);
};

export default SettingsDialog