import React, {Component} from 'react';
import Pixel from "./Pixel";
import {pointFromKey} from "../../helpers/KeyDrawerHelper";

let mouseDown = 0;

let isRMB = false;

document.oncontextmenu = e => {
    e.preventDefault();
    e.stopPropagation();
};

document.onmousedown = e => {
    mouseDown++;
    console.log('mouse down');
    isRMB = checkRMB(e);
    if (isRMB) {
        e.preventDefault();
        e.stopPropagation();
    }
};
document.onmouseup = () => {
    mouseDown--;
    isRMB = false;
};

const isMouseDown = () => mouseDown > 0;

document.onkeydown = e => {
    if (e.shiftKey && e.metaKey && e.key === 'f') {
        console.log('worked');
        mouseDown = 0;
    }
};

const checkRMB = e => {
    let isRightMB;

    if ("which" in e)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
        isRightMB = e.which === 3;
    else if ("button" in e)  // IE, Opera
        isRightMB = e.button === 2;

    return isRightMB || e.type === 'contextmenu';
};

class DrawPaper extends Component {

    paper = React.createRef();

    handleDraw = e => {
        if (isMouseDown())
            this.props.onDraw(
                e.clientX - this.paper.current.offsetLeft,
                e.clientY - this.paper.current.offsetTop,
                isRMB);
    };


    render() {
        console.log('mouse', mouseDown);
        const {size, picture, settings} = this.props;

        const {W, H, withBorders, black} = settings;

        const pixelSize = size / W;

        return (<div
            ref={this.paper}
            onMouseMove={this.handleDraw}
            style={{
                backgroundColor: black ? 'black' : 'white',
                border: 'solid 0.2px grey',
                position: 'relative',
                width: size,
                height: size
            }}>
            {Object.keys(picture).map(pos =>
                <Pixel key={pos}
                       black={black}
                       position={pointFromKey(pos)}
                       value={picture[pos]}
                       size={pixelSize}
                       withBorders={withBorders}/>)
            }
        </div>);
    }
}

export default DrawPaper;