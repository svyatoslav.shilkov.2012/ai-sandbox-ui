import React, {Component} from "react";

export default class Pixel extends Component {

    shouldComponentUpdate(nextProps) {
        return nextProps !== this.props;
    }

    render() {
        const {value, size, withBorders, position, black} = this.props;
        const c = 255 * (black ? value : 1 - value);

        const borderWidth = 0.5;
        const margin = 2;
        const side = size - 2 * margin - (withBorders ? borderWidth * 2 : 0);

        return (<div
            style={{
                borderRadius: 50,
                position: 'absolute',
                borderColor: 'grey',
                borderStyle: 'solid',
                borderWidth: withBorders ? borderWidth : 0,
                width: side,
                height: side,
                margin,
                left: position ? position.x * size : 'default',
                top: position ? position.y * size : 'default',
                backgroundColor: `rgb(${c},${c},${c})`,
                userSelect: 'none',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                color: (black ? value < 0.5 : value > 0.5) ? 'white' : 'black'
            }}>{(value + '').substring(0, 3)}</div>);
    }
}
