import React, {Component} from 'react';

class Cdiv extends Component {
    render() {
        return (
            <div style={{
                display: 'flex',
                justifyContent: this.props.x ? 'center' : 'default',
                alignItems: this.props.y ? 'center' : 'default',
                ...this.props.style
                 }}>
                {this.props.children}
            </div>
        );
    }
}

export default Cdiv;