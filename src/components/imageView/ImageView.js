import {makeStyles} from "@material-ui/styles";
import React from "react";

const useStyles = makeStyles({
    root: props => ({
        position: 'relative',
        width: props.size,
        height: props.size,
        border: 'solid grey 1px',
        backgroundImage: `url(${props.image})`,
        backgroundSize: '100%',
        backgroundRepeat: 'no-repeat',
    })
});

const ImageView = (props) => {
    const classes = useStyles(props);
    return <div className={classes.root}/>;
};

export default ImageView;