import React from "react";
import {styled} from "@material-ui/styles";
import makeStyles from "@material-ui/styles/makeStyles";

const useStyles = makeStyles({
    pix: props => {
        const c = (1 - props.value) * 255;
        return {
            position: 'absolute',
            left: props.size * props.i,
            top: props.size * props.j,
            width: props.size,
            height: props.size,
            backgroundColor: `rgb(${c},${c},${c})`
        }
    }
});

const ImgPixel = (props) => {
    const classes = useStyles(props);
    return <div className={classes.pix}/>;
};

export default ImgPixel;