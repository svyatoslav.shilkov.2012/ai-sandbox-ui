import React from "react";
import {makeStyles} from "@material-ui/styles";
import ImgPixel from "./ImgPixel";

const useStyles = makeStyles({
    root: props => ({
        position: 'relative',
        width: props.size,
        height: props.size,
        border: 'solid grey 1px',
    })
});

const ArrayImageView = (props) => {
    const classes = useStyles(props);
    const pixSize = props.size / props.image.length;
    return (<div className={classes.root}>{
        props.image.map((row, i) =>
            row.map((pixValue, j) => <ImgPixel size={pixSize}
                                               value={pixValue}
                                               i={i} j={j}/>)
        )
    }</div>);
};

export default ArrayImageView;