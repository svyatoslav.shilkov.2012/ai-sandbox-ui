import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Router} from "react-router-dom";
import CustomHistory from "./CustomHistory";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import theme from "./theme";

ReactDOM.render(
    <Router history={CustomHistory}>
        <ThemeProvider theme={theme}>
            <App/>
        </ThemeProvider>
    </Router>
    , document.getElementById('root'));

