Array.prototype.max = function(){
    return this.reduce((p, c) => Math.max(p, c), -Infinity);
};

export const sqr = v => v * v;

export const dist = (p1, p2) => Math.sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y));

export const diag = side => Math.sqrt(2 * sqr(side));

export const safeDiv = (a, b) => b == 0 || a / b === Infinity ? 0 : a / b;