import {point} from "./Helper";

export const emptyObjPicture = (w, h) => {
    const obj = {};
    for (let i = 0; i < h; ++i) {
        for (let j = 0; j < w; ++j)
            obj[`${i}-${j}`] = 0;
    }
    return obj;
    // new Array(h).fill(Array(w).fill(0)).;
};


export const pointFromKey = posKey => {
    const sepInd = posKey.indexOf('-');
    const x = Number.parseInt(posKey.substring(0, sepInd));
    const y = Number.parseInt(posKey.substring(sepInd + 1));
    return point(x, y);
};


export const keyFromPoint = point => `${point.x}-${point.y}`;


export const objPictureToArray = objPicture => {
    const points = Object.keys(objPicture).map(pointFromKey);
    const w = points.map(p => p.x).max();
    const h = points.map(p => p.y).max();
    return new Array(h + 1).fill().map((er, y) =>
        new Array(w + 1).fill().map((ee, x) => objPicture[keyFromPoint(point(x, y))])
    );
};