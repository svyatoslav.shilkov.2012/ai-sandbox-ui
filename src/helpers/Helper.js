export const deepCopy = obj => JSON.parse(JSON.stringify(obj));

export const toInt = str => Number.parseInt(str) || 0;

export const toDouble = str => Number.parseFloat(str) || 0;

export const point = (x, y) => ({x, y});

export const pair = (k, v) => {
    const obj = {};
    obj[k] = v;
    return obj;
};

