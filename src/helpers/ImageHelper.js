export const base64ToPixelArray = base64str => new Promise((resolve, reject) => {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    const image = new Image();
    image.onload = () => {
        context.drawImage(image, 0, 0, image.width, image.height);
        const pixelArray = new Array(image.height).fill().map(() => new Array(image.width).fill(0));
        const imData = context.getImageData(0, 0, image.width, image.height);
        for (let i = 0; i < image.height; ++i) {
            for (let j = 0; j < image.width; ++j) {
                pixelArray[i][j] = imData.data[(i * image.height + j) * 4] / 255;
            }
        }
        resolve(pixelArray);
    };
    image.src = base64str;
});


export const pixelArrayToPNG = pixelArray => {

    const w = pixelArray.length;
    const h = pixelArray[0].length;

    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    const imgData = context.createImageData(w, h);

    canvas.height = w;
    canvas.width = h;

    pixelArray = pixelArray.flat();


    for (let i = 0; i < pixelArray.length; i++) {
        imgData.data[i * 4] = pixelArray[i] * 255;
        imgData.data[i * 4 + 1] = pixelArray[i] * 255;
        imgData.data[i * 4 + 2] = pixelArray[i] * 255;
        imgData.data[i * 4 + 3] = 255;
    }

    context.putImageData(imgData, 0, 0);

    const src = canvas.toDataURL('image/png');
    console.log(src);
    return src;
};

export const inverse = pixelArray => pixelArray.map(row => row.map(px => 1 - px));