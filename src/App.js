import React, {Component} from 'react';
import {Redirect, Route, Switch} from "react-router-dom"
import TestModel from "./pages/testModel/TestModel";
import TrainModel from "./pages/trainModel/TrainModel";
import FrontendRoutes from "./data/constants/FrontendRoutes";


export default class App extends Component {

    render() {
        return <div>
            <Switch>
                <Route exact path={FrontendRoutes.TEST} component={TestModel}/>
                <Route exact path={FrontendRoutes.TRAIN} component={TrainModel}/>
                <Redirect to={FrontendRoutes.TEST} />
            </Switch>
        </div>
    }
}

