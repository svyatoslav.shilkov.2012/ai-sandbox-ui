import React, {Component} from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

class ZoomableCanvasChart extends Component {

    // componentDidUpdate(prevProps){
    //     console.log(prevProps.data);
    //     if(this.props.data.length > prevProps.data.length)
    //         this.chart.render();
    // }

    render() {

        const {title, xTitle, yTitle, data, series, xLabel} = this.props;

        const options = {
            theme: "light1", // "light1", "dark1", "dark2"
            animationEnabled: true,
            zoomEnabled: true,
            title: {text: title},
            axisY: {
                title: yTitle,
                includeZero: true
            },
            axisX: {
                title: xTitle
            },
            toolTip: {
                shared: true
            },
            data: series.map(seriesName => {
                const seriesData = data.map(p => ({label: p[xLabel], y: p[seriesName]}));

                return {
                    type: "spline",
                    name: seriesName,
                    showInLegend: true,
                    dataPoints: seriesData
                }
            })
        };


        return (
            <div>
                <CanvasJSChart options={options} onRef={ref => this.chart = ref}/>
            </div>
        );
    }
}

export default ZoomableCanvasChart;