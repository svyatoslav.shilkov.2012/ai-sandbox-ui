import React, {PureComponent} from 'react';
import ZoomableCanvasChart from "./ZoomableCanvasChart";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import Progress from "./Progress";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import {deepCopy} from "../../helpers/Helper";
import Box from "@material-ui/core/Box";
import Input from "@material-ui/core/Input";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Api from "../../data/api/Api";
import Paper from "@material-ui/core/Paper";

// import ZoomableChart from "./ZoomableChart";


const generateData = size => new Array(size).fill().map((any, step) => ({
    step,
    accuracy: Math.random(),
    loss: Math.random()
}));

const ActivationFunc = {
    Sigmoid: "Sigmoid",
    ReLU: "ReLU",
    Leaky_ReLU: "Leaky_ReLU",
    ELU: "ELU",
};


export default class TrainModel extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            data: generateData(100),
            progress: {
                value: 0,
                curLoss: 1,
                curAcc: 0,
                totalAcc: 0
            },
            progressData: [],
            params: {
                structure: '16:16',
                activationFunc: ActivationFunc.ReLU,
                withSoftMax: true
            },
            train: {
                learningRate: 0.1,
                epochs: 3,
                batchSize: 200
            },
            results: []
        };
    }

    componentDidMount() {
        Api.getExampleNet().then(res => this.setState({results: [res.data]}))
    }

    handleStructChange = e => this.changeState('params','structure', e.target.value);
    handleActivationChange = e => this.changeState('params', 'activationFunc', e.target.value);
    handleSoftMaxSelect = e => this.changeState('params', 'withSoftMax', e.target.checked);


    handleAlphaChange = e => this.changeState('train','learningRate','0.1');
    handleEpochsChange = e => this.changeState('train','epochs','3');
    handleBatchSizeChange = e => this.changeState('train','batchSize','200');


    changeState = (param1, param2, value) => {
        const state = deepCopy(this.state);
        state[param1][param2] = value;
        this.setState(state);
    };

    startTraining = () => {

    };

    render() {
        const {progress, progressData, params, train, results} = this.state;

        const st = {
            activFuncRoot: {
                margin: 10
            },
            trainBtn: {
                margin: '2%',
                padding: '2%',
                backgroundColor: 'green',
                color: 'white',
                width: '90%',
                fontSize: 25
            }
        };

        return (
            <Container>
                <Grid container spacing={3}>
                    <Grid item lg={3} md={6}>
                        <Typography variant='h3'>Params</Typography>
                        <Typography>Structure</Typography>
                        <Box display="flex" alignItems="center">
                            <Typography>784:</Typography>
                            <Input inputProps={{style: {textAlign: 'center'}}} value={params.structure} onChange={this.handleStructChange}/>
                            <Typography>:10</Typography>
                        </Box>
                        <FormControl fullWidth style={st.activFuncRoot} component="fieldset">
                            <FormLabel component="legend">Activation function</FormLabel>
                            <RadioGroup aria-label="activationFunc"
                                        name="activationFunc"
                                        value={params.activationFunc}
                                        onChange={this.handleActivationChange}>
                                {Object.values(ActivationFunc).map(func =>
                                    (<FormControlLabel value={func}
                                                       label={func}
                                                       control={<Radio/>}
                                    />))
                                }
                            </RadioGroup>
                        </FormControl>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={params.withSoftMax}
                                    onChange={this.handleSoftMaxSelect}
                                    name="softMax"
                                    color="primary"
                                />
                            }
                            label="With SoftMax on output"
                        />
                    </Grid>
                    <Grid item lg={2} md={6}>
                        <Typography variant='h3'>Train</Typography>
                        <TextField fullWidth value={train.learningRate} onChange={this.handleAlphaChange} label='Learning Rate'/>
                        <TextField fullWidth value={train.epochs} onChange={this.handleEpochsChange} label='Epochs'/>
                        <TextField fullWidth value={train.batchSize} onChange={this.handleBatchSizeChange} label='Batch size'/>
                        <Button variant='contained'
                                style={st.trainBtn}
                                onClick={this.startTraining}>TRAIN</Button>
                    </Grid>
                    <Grid item lg={6} md={12}>
                        <Typography variant='h3'>Results</Typography>
                        {/*{results.map(r => (<Paper>*/}
                        {/*    <Grid>*/}
                        {/*        <Grid item><Typography>Structure={r.structure}</Typography></Grid>*/}
                        {/*    </Grid>*/}
                        {/*</Paper>)}*/}
                    </Grid>
                </Grid>
                <Progress variant="determinate" value={progress.value}/>
                <ZoomableCanvasChart title='Training progress'
                                     data={progressData}
                                     series={['accuracy', 'loss']}
                                     xLabel='step'/>
            </Container>
        );
    }
}
