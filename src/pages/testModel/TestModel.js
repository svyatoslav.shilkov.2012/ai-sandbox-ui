import React, {Component} from 'react';
import KeyDrawer from "../../components/keyDrawwer/KeyDrawer";
import {toDouble} from "../../helpers/Helper";
import ImageView from "../../components/imageView/ImageView";
import {inverse, pixelArrayToPNG} from "../../helpers/ImageHelper";
import axios from 'axios';
import {Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import Api from "../../data/api/Api";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import FrontendRoutes from "../../data/constants/FrontendRoutes";
import Button from "@material-ui/core/Button";

const roundD = (v, prec) => toDouble((v + '').substring(0, 3));

const round = arr => arr.map(r => r.map(roundD));

const Digits = ({predictions}) => {

    const data = predictions.map(([digit, value]) => ({digit, value}));

    return (
        <ResponsiveContainer width={'100%'} height={'100%'}><BarChart
        data={data}
        margin={{
            top: 20, right: 0, left: 0, bottom: 0,
        }}
    >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="digit" />
        <YAxis dataKey="value" allowDecimals/>
        <Tooltip />
        <Bar dataKey="value" fill="#82ca9d" />
    </BarChart></ResponsiveContainer>);
};

const UPDATE_FPS = 2;
const CALL_DELAY = 1000 / UPDATE_FPS;

class TestModel extends Component {

    componentDidMount() {
        document.title = 'Page One';
    }

    state = {
        images: [],
        predictions: new Array(10).fill(0).map((e, i) => [i, e])
    };

    saveImg = pixelArrayImg => {
        console.log(round(pixelArrayImg));
        const base64Png = pixelArrayToPNG(inverse(pixelArrayImg));
        this.setState({images: [...this.state.images, base64Png]});
    };

    lastCall = 0;
    onDraw = pic => {
        const cur = Date.now();
        if (cur - this.lastCall > CALL_DELAY) {
            Api.classify(pic).then(res => this.setState({predictions: res.data.digits}));
            this.lastCall = cur;
        }
    };

    render() {
        return (
            <Grid style={{display: 'flex'}}>
                <Grid container spacing={5}>
                    <Grid item lg={6}>
                        <KeyDrawer size={700} onSave={this.saveImg} onDraw={this.onDraw}/>
                    </Grid>
                    <Grid item lg={6} style={{width: 600, height: 250, padding: 10, marginTop: 50}}>
                        <Digits predictions={this.state.predictions}/>
                        <Button style={{margin: 50}} component={Link} to={FrontendRoutes.TRAIN}>Train</Button>
                    </Grid>
                </Grid>
                <Grid style={{
                    overflowY: 'auto',
                    height: '90vh',
                    width: 220,
                    flex: 'none'
                }}>{this.state.images.map((img, i) =>
                    <div key={'' + i} style={{margin: 10}}><ImageView size={100} image={img}/></div>
                )}</Grid>
            </Grid>
        );
    }
}

export default TestModel;