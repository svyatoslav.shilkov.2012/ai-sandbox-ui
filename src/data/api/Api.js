import axios from 'axios';
import Endpoints from "../constants/Endpoints";

class Api{

    getExampleNet = () => axios.get(Endpoints.GET_EXAMPLE_NET);

    classify = picture => axios.post(Endpoints.CLASSIFY, {data: picture})
}

export default new Api();