const FrontendRoutes = {
    TRAIN: '/train_model',
    TEST: '/test_model'
};

export default FrontendRoutes;