const Endpoints = {
    GET_EXAMPLE_NET: '/api/example_net',
    CLASSIFY: '/api/classify'
};

export default Endpoints;